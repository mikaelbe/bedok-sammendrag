\section{Driftsregnskapet basert på normalkalkulasjon}

For den løpende økonomiske styringen er vi avhengig av å sette opp forkalkyler. Dette handler om målsettinger, planer og forventninger til en gitt fremtidig periode. Disse skal innarbeides og sammenliknes i det som blir budsjettet. Vi skal i dette kapittelet stifte kjenskap til normalkalkulasjon.

\subsection{Driftsbudsjettering}

Budsjettene har til formål å samle organisasjonen om felles mål og sørge for en koordinert og rasjonell gjennomføring av de planlagte tiltakene som skal lede til måloppnåelse. Budsjettoppstillingen er et tallmessig uttrykk for en bedrifts handlingsplan for en gitt fremtidig periode.

\emph{Produksjonsbudsjettet} i en tilvirkingsbedrift består av
\begin{center}
	\begin{tabular}{ c l }
		~ & Budsjetterte direkte materialer \\
		+ & Budsjettert direkte lønn \\
		+ & Budsjetterte indirekte tilvirkingskostnader \\
		\hline
		= & Budsjetterte tilvirkingskostnader
	\end{tabular}
\end{center}

For å kunne budsjettere driftskostnadene er utgangspunktet det budsjetterte produksjonsvolumet, som er:
\begin{center}
	\begin{tabular}{ c l }
		~ & Årets budsjetterte salg i antall enheter \\
		+ & IB \\
		+ & UB \\
		\hline
		= & Årets budsjetterte produksjon i antall enheter
	\end{tabular}
\end{center}

Budsjettering av de direkte kostnadene er her definert som budsjettering av direkte materialer og direkte lønn. På basis av produktkalkyler kan de direkte kostnadene budsjetteres ved å multiplisere enhetskalkylene for direkte materialer og direkte lønnskostnader med den planlagte produksjonsmengden.

De indirekte variable kostnadene omfatter forbruket av driftsmateriell og hjelpemateriell. De blir ofte budsjettert i sum ut i fra det budsjetterte aktivitetsnivå, som produksjonsvolum, sum maskintimer, sum direkte timer.

Budsjettering av de indirekte faste kostnadene innebærer kjente kostnader og et gitt kapasitetsintervall, og er faste innenfor intervallet. Før vi kan budsjettere dem må derfor årets budsjetterte produksjonsvolum omregnes til nødvendig produksjonskapasitet, nedbrutt på de enkelte av bedriftens avdelinger. 

\subsection{Forkalkyler i divisjons- og ekvivalenskalkulasjon}

Vi kan gjøre dette for virksomheter som produserer ensartede produkter eller har ensartede produksjonsprosesser å fordele kostnadene på. For å fine selvkost, alternativt tilvikningsmerkost, deler vi de samlede kostnadene på antall produserte enheter eller på ekvivalensenheter. Vi benytter budsjetterte tall.

\subsection{Forkalkyler i tilleggskalkulasjon}

Kalkylen utarbeides på samme måte som i etterkalkulasjon, men med kostnader, aktivtetsmål og fordelingsgrunnlag som budsjetterte størrelser. Utfordringen blir å bryte ned faste indirekte kostnader slik at vi kan knytte dem til de enkelte produkter eller ordre slik at hvert enkelt produkt eller ordre bærer sin rettmessige andel av kostnadene.

Tilleggsatser brukt i en forkalkyle eller et budsjett kalles \emph{normalsatser}. Navnet kommer fra at de beregnes ut fra avdelingenes budsjetterte aktivitet, som vi kaller normalproduksjonen. Den budsjetterte \emph{normalproduksjonen} blir brutt ned på avdelingene.

Eksempel: En avdeling budsjetterer med 1500 maskintimer, og indirekte faste kostnader for perioden er 600 000 kr. Normalsatsen blir $600000/1500 = 400$ kr per maskintime.

\subsection{Normalkostregnskapet}

Normalkostregnskapet baserer seg på normalkalkulasjon og har fokus på hvor store de indirekte kostnadene skal være. Det benyttes av typiske ordreproduserende virksomheter som skreddersyr en eller noen få spesialmaskiner eller større konstruksjoner for kunder. Det har et kortperiodisk perspektiv ved at det på månedsbasis følger opp kostnadsutviklingen.

Direkte lønn og materialer registreres i driftsavdelingene der de realiseres, på grunnlag av de enkelte ordrer eller produkter, mens indirekte kostnader innkalkuleres på grunnlag av normalsatsene og virkelig aktivitet. Vi får da en målestokk på hvor store de indirekte kostnadene skal være.

\subsection{Normalkostregnskapet etter selvkostprinsippet}

Det som skiller normalkostregnskapet fra et tradisjonelt historisk kostregnskap, er at kostnadsfordelingen fra avdelingene til produkter eller ordrer skjer vha normalsatsene.
\begin{center}
	\begin{tabular}{ c l }
		~ & Salgsinntekter \\
		- & Kalkulert selvkost solgte varer \\
		\hline
		= & \emph{Produktresultat} \\
		+ & Dekningsdifferanser \\
		\hline
		= & \emph{Produksjonsresultat}
	\end{tabular}
\end{center}

\noindent
I boken følger et stort eksempel for normalkostregnskap vha selvkost- og bidragsprinsipp. For normalkostregskapet etter selvkostprinsippet fremkommer kalkulert resultat for hvert produkt, og resultatet betegnes som \emph{produktresultatet}. For det virkelige resultatet i perioden brukes betegnelsen \emph{produksjonsresultat}. 

\subsection{Avviksanalyse ved selvkost}

Det er sjelden at virkeligheten blir som forutsatt. Det vil oppstå dekningsdifferanser når vi senere sammenlikner etterkalkylen mot forkalkylen. Dekningsdifferansen er de innkalkulerte indirekte kostnadene, basert på normalsatser, minus de virkelig forbrukte indirekte kostnadene. Det største avviket er vanligvis knyttet til de indirekte faste kostnadene, som i prinsippet er upåvirket av kapasitetsutnyttelsen i intervallet. Jo høyere produksjon, jo lavere blir enhetskostnadene.

Avvikene i indirekte faste kostnader er sammenstatt av to separate avvik: volumavviket og forbruksavviket.

Volumavviket skyldes at vi har behandlet kostnadene som om de var variable, mens de i virkeligheten var faste. Dette kaller vi overdekning (f.eks flere maskintimer enn normalproduksjon) eller underdekning (f.eks færre maskintimer enn normalproduksjon). Volumavviket er de innkalkulerte faste kostnadene minus normalkostnadene, gitt budsjettet.

Forbruksavviket omhandler husværekostnader, kraftforbruk og indirekte lønnskostnader. I de indirekte variable kostnadene oppstår det kun forbruksavvik. Forbruksavviket er gitt som normalkostnader, fra budsjettet, minus de virkelige faste kostnadene.

\subsection{Normalkostregnskapet etter bidragsprinsippet}

Her er det bare de variable kostnadene som skal belastes. Periodens tilvirkings\emph{mer}kost utgjør summen av virkelige direkte material- og lønnskostnader og de inkalkulerte indirekte variable tilvirkingskostnader. 

\subsection{Dekningsdifferansene i bidragsregnskapet}

Siden de faste indirekte kostnadene belastes de enkelte periodene i sin helhet, uavhengig av aktivitetsnivå, oppstår ikke noe volumavvik i bidragsregnskapet. Dekningsdifferansen som oppstår er dermed kun forbruksavvik.

\subsection{Resultatsammenhengen selvkost- og bidragsregnskapet}

I boken følger vi et eksempel. I perioder med beholdningsøkning vil selvkostregnskapet vise bedre resultater enn bidragsregnskapet. Også motsatt. 