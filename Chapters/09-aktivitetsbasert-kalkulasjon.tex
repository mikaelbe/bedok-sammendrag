\section{Aktivitetsbasert kalkulasjon (ABC) -- begreper og prinsipper}

Aktivitetsbasert kalkulasjon, eller \emph{Activity Based Costing} (ABC), ble introdusert på slutten av 1980-tallet, som et verktøy for bedre å kalkulere produktkostnader i industribedrifter. Senere er konseptet utviklet både til å anvendes i flere sammenhenger og for flere typer virksomheter.

Formålet er å bruke aktivitetsbasert informasjon for styringsformål. Tidligere versjoner fokuserte på produktkostnader, men i senere vrsjoner er fokus flyttet til hvordan aktiviteter driver ressursbruken og hvordan denne kan påvirkes. I dag anvendes \emph{aktivitetsbasert styring} eller \emph{Acitivity Based Management} som betegnelse. Konseptet gjelder ikke bare serieproduserende bedrifter, men også tjenesteytende virksomheter. Det ser man også i Norge, der noen av de største ABC-prosjektene har vært i tjenesteytende sektor. For eksempel har både DNB og Posten store ABC-prosjekter.

\subsection{ABC -- hva er D?}

Med økt konkurranse får bedrifter press på marginene. Dette er en av de viktigste årsakene til at man innfører avanserte kalkyler. Man ønsker å vite hvor man tjener penger, og hvor man taper. Dette gjelder industri såvel som privat og offentlig tjenesteproduksjon. Man ser interesse for ABC også i sykehus, kommuner og offentlige transportselskap.

I mange virksomheter er kunder og kundelønnsomhet viktigere enn fokus på kostnader og egen lønnsomhet. I neste kapittel ser i derfor på kundelønnsomhet. Det kan også være lurt å analysere lønnsomheten av ulike distribusjonskanaler, som om det er mest lønnsomt å selge mobilabonnement over ineternett, i egne butikker eller gjennom avtaler med frittstående selgere.

Det er altså mange ulike kalkyleobjekter som er aktuelle. Dette er et sentralt poeng i ABC, og vi definerer \emph{kalkyleobjekt} som:

\emph{Et kalkyleobjekt er det vi ønsker å beregne eller måle kostnaden for: et produkt, et prosjekt, en tjeneste, en avdeling, en kunde, en kundegruppe, et marked, en distribusjonskanal eller et program, som alle har det til felles at de forbruker ressurser.}

\begin{figure}
	\caption{ABC-modell}
	\label{fig:abc-modell}
	\centering
		\includegraphics[width=0.8\textwidth]{Figures/abc-modell}
\end{figure}

Figur \ref{fig:abc-modell} illustrerer tostegsprinsippet i kostnadsfordelingen. Kalkyleobjektene, for eksempel konder og tjenester, forbruker noen direkte ressurser (f.eks. direkte material), mens hovedtyngden av ressursforbruke skjer ofte indirekte via aktiviteter. Fordelingens første steg er å henføre kostnader til aktiviteter basert på hvordan aktivitetene legger beslag på ressurser.

For hver aktivitet kan det identifiseres en kostnadsdriver. Basert på aktivitetens ressursforbruk og måling av kostnadsdriveren, fastsettes kostnaden per enhetsdriver. Så beregnes det en kostnad. Denne kostnaden ligger til grunn for steg to: kostnadsfordelingen fra aktiviteter til kalkyleobjektene. ABC-prinsippet kan sees som en regning for å utnytte aktiviteter der kalkyleobjektet, for eksempel et produkt, belastes med kostnader basert på antall enheter av kostnadsdriveren ganget med prisen man har satt på en kostnadsdriverenhet.

De grunnleggende prinsippene kan eskrives som følger:
\begin{enumerate}
  \item ABC beskriver ressursbruken i form av \emph{aktiviteter} i motsetning til tradisjonelle ansvarsstedsbaserte inndelinger som avdelinger.
  \item ABC fokuserer på hvilke faktorer som driver kostnadene i virksomheten. En slik faktor kalles en \emph{kostnadsdriver}. En kostnadsdriver kan være mer enn produksjonsvolumet eller andre volumbaserte faktorer som arbeidstimer og maskintimer.
  \item ABC legger spesielt vekt på at \emph{kompleksitet} driver kostnader, altså at det koster å produsere mange produkter fordi man da får mer planlegging, koordineringsarbeid, innkjøp, lagerhold og liknende.
  \item ABC søker å \emph{skille ut ledig kapasitet} ved å ikke belaste kalkyleobjekter (for eksempel produktene og kundene) med kostnader for ubenyttet kapasitet.
\end{enumerate}

\subsection{Stegene i en ABC-kalkyle}

Hovedprinsippet er å spore kostnader til produkter, produktgrupper, kunder eller andre objekter via aktiviteter. Dette utføres normalt i følgende steg:

\begin{enumerate}
  \item \emph{Identifisering av aktiviteter som utføres}. Man setter opp en liste av aktiviteter, som bør beskrive virksomheten på en best mulig måte, men samtidig ikke \emph{for} detaljert.
  \item \emph{Fordeling av kostnader til aktiviteter, eventuelt via hjelpeaktiviteter}. Ofte baseres dette på at man registrerer hvor mey tid og andre ressurser som går med til en aktivitet. For enkelte aktiviteter kan man trenge støtte av andre aktiviteter, som igjen fordeles til hovedaktiviteten. For eksempel kan IT-støtte fordeles ut på forelesere når man skal finne ut hva undervisningene koster på en høgskole.
  \item \emph{Identifisering av kostnadsdrivere og valg av fordelingsnøkler.} For hver aktivitet bestemmer man en faktor som skal ligge til grunn for kostnadsfordelingen. Fordelingen bør sammenfalle med det som driver kostnaden (kostnadsdriveren), men av og til kan det være krevende å måle antall enheter av kostnadsdriveren. På en operasjonsstue kan for eksempel antall timer være kostnadsdriveren, men krevende å måle. Ofte fordeler man derfor kostnader basert på antall operasjoner av ulike kategorier. Dette for å forenkle arbeidet.
  \item \emph{Fordeling av kostnader til produkt eller annet objekt.} Dette krever at man måler hvert produkts forbruk av fordelingsnøkkelenheter. Skal man for eksempel fordele undervisningskostnader til ulike studier, må man måle hvor mange undervisningstimer som går med til hvert studium.
\end{enumerate}

Disse stegene kan være like andre typer kalkyler. Hovedforskjellen er at man ikke direkte kan basere seg på avdelingsregnskapet eller annet lett tilgjengelig datagrunnlag, men trenger flere spesifiseringer av aktiviteter, kostnadsdrivere og forbruk av fordelingsnøkler.

\subsection{Gruppering av kostnader i aktiviteter}

Alle virksomheter er basert på at det utføres arbeidsprosesser. Disse beskrives som aktiviteter. Aktiviteter kan defineres som \emph{én eller en gruppe avgrensede, gjentatte arbeidsprosesser}. Når mans kal kartlegge ressursbruk på aktiviteter bør man legge vekt på
\begin{enumerate}
  \item \emph{Delbarhet.} Det måv ære mulig å skille ressursbruken i én aktivitet fra ressursbruken for alle andre aktiviteter. Dette kan være problematisk ved gjensidig avhengighet av aktiviteter.
  \item \emph{Likartethet med hensyn til driveren av kostnadene i kostnadsgruppen.} ABC fokuserer på hva som driver kostnadene og at man kan skille kostnader drevet av en faktor fra kostnadene drevet av andre faktorer. Vi bør derfor ikke ha mer enn én driver.
  \item \emph{Styringsmessig meningsfull inndeling.} Målsettingen med grupperinen er å bidra til å øke forståelsen av hva ressursene brukes til.
\end{enumerate}

Typisk vil en avdeling inneholde mer enn én aktivitet, og aktiviteter kan også gå på tvers av adelinger. Et nærliggende eksempel av aktiviteter på tvers av avdelingene e undervisning, veiledning, eksamensretting og studieadministrasjon. Alle kan styres av en avdeling, men har ulike kostnadsdrivere, og er styringsmessig meningsfulle å skille ut.

Det kan være hensiktsmessig å gruppere aktiviteter i aktivitetsgrupper.

\subsection{Fordeling av kostnader basert på hva som driver dem}

Når man fordeler kostnader til kalkyleobjekter ser vi på årsaken til at kostnaden oppstår. Dette kaller vi en kostnadsdriver:

\emph{Kostnadsdriveren er den faktoren som på lang sikt er dimensjonerende for en aktivitet.}

Å finne kostnadsdriveren er ikke alltid lett.Ser vi på en barneskole, så er den viktigste driveren antall klasser eller grupper elevene er delt inn i. Antall elever i hver gruppe vil også kunne være drivere. Vi har derfor flere kostnadsdrivere, men må ofte velge én når kostnader fordeles. Skal man da velge antall klasser, antall elever eller en kombinasjon som fordelingsnøkkel? Det viktigste er å velge den faktoren som på lang sikt forklarer svingninger i totale kostnader. I dette tilfellet kan det være antall klasser, da det er den viktigste faktoren for å bestemme antall lærere.

Vi skiller mellom forskjellige typer kostnadsdrivere:
\begin{enumerate}
  \item \emph{Frekvensbaserte (transaksjonsbaserte):} Antall ganger en aktivitet gjennomføres, for eksempel antall billetter solgt eller antall innkjøp. Diss måle hvor mange ganger en aktivitet utføres i en gitt periode.
  \item \emph{Varighetsbaserte:} For en del aktiviteter kan man måle hvor lang tid man bruker en aktivitet, for eksempel antall operasjonstimer for en spesiell type operasjoner på en operasjonsstue. Tidsbruken ganges så med en timepris.
  \item \emph{Direkte ressursbruksbaserte:} Disse måler aktivitetes ressursbruk for eksempel gjennom observasjon av den arbeidsoppgave som utføres. På en operasjonsstue kan man registrere all ressursbruk knyttet til en enkelt operasjon, ikke bare hvor lang tid den tar. Man kan registrere forbruk av materiell, sykepleiertid, anestesitid, kirurgtid også videre.
\end{enumerate}

Eksempler på kostnadsdrivere kan være:
\begin{center}
	\begin{tabular}{l l l}
	Virksomhet			& Aktivitet					& Kostnadsdriver \\
	\hline
	Sykehus				& Operasjon					& Operasjonstimer \\
	Høgskole			& Eksamensretting			& Antall besvarelser/rettetid \\
	Kino				& Billettsalg				& Antall solgte billetter \\
	Sengetøysprodusent	& Produksjonsomstilling		& Antall ommstillinger \\
	Bildelsprodusent	& Utvikling av støtfanger	& Antall utviklingstimer
	\end{tabular}
\end{center}

\subsection{Eksempel på ABC i sykehus}

Anta at man har følgende fordeling av lønnskostnader per år for en øye og øre-nese-halsavdeling:

\begin{center}
	\begin{tabular}{l r}
		Kostnader				& Beløp \\
		\hline
		Innleggelseskostnader	& kr 5 000 000 \\
		Behandlingskostnader	& kr 13 000 000 \\
		Pleiekostnader			& kr 20 000 000 \\
		Utskrivingskostnader	& kr 3 000 000 \\
		\hline
		Totale kostnader		& kr 41 000 000 \\
	\end{tabular}
\end{center}

Avdelingen vurderer 2 ulike systemer for å fordele lønnskostnader til pasientgrupper:
\begin{enumerate}
  \item Alle kostnader fordeles ut basert på liggetid (totalt 10 000 liggedøgn).
  \item Et tilnærmet ABC-system hvor man bruker fire kategorier på aktiviteter:
  \begin{center}
  	\begin{tabularx}{\linewidth}{X X X}
  		Aktivitetsgruppe		& Kostnader som inngår 		& Kostnadsdriver \\
  		\hline
  		Innleggelsesrelaterte kostnader
  		& Aktiviteter knyttet til innleggelse og utskriving
  		& Antall pasienter
  		\\
  		\hline
  		Liggedøgnsrelaterte kostnader
  		& 50 \% av gruppen ``pleiekostnader''
  		& Antall liggedøgn
  		\\
  		\hline
  		Pleievektsrelaterte kostnader
  		& Behandlingskostnader + 50 \% av ``pleiekostnader''
  		& Antall pleievekter
  		\\
  		\hline
  		Sykehusnivå kostnader
  		& Øvrige kostnader
  		& Periodekostnader
  		\\
  		\hline
  	\end{tabularx}
  \end{center}
\end{enumerate}

Merk at pleiekostnader både er drevet av liggetid og pleievekter. Avdelingen har 2 000 innleggelser, 10 000 liggedøgn og produserte 20 000 pleievekter under normale omstendigheter.

For å teste systemet ut, har man valgt følgende pasientgrupper med tilhørende verdier for liggetdi og pleievekter.

\begin{center}
	\begin{tabular}{l c c c}
		Behandling	& Grå stær & Fj. av mandler & Laryngektomi \\
		\hline
		Gj.snitt liggedøgn per pasient		& 5 & 2 & 17 \\
		Gj.snitt pleievekter per pasient	& 7 & 4 & 24 \\
	\end{tabular}
\end{center}

Dersom alle kostnadene fordeles ut per liggedøgn, får vi
\begin{equation}
	\frac{\text{kr 41 000 000}}{\text{10 000 liggedøgn}} = \text{kr 4 100 per liggedøgn}
\end{equation}

Kostnader for de ulike pasientgruppene blir da
\begin{center}
	\begin{tabular}{c c c}
		Grå stær & Fjerning av mandler & Laryngektomi \\
		\hline
		kr 20 500 & kr 8 200 & kr 69 700
	\end{tabular}
\end{center}

I ABC-systemet må vi først finne en kostnad per kostnadsdriverenhet:
\begin{center}
	\begin{tabularx}{\linewidth}{| X | X | X | X |}
		\hline
		Aktivitetsgruppe & Kostnad & Antall enheter av driveren & Kostnad per kostnadsdriverenhet \\
		\hline
		Innleggelsesrelaterte aktiviteter & kr 8 000 000 & 2 000 innleggelser & 4 000 per innleggelse \\
		\hline
		Liggedøgnsrelaterte aktiviteter & kr 10 000 000 & 10 000 liggedøgn & 1 000 per liggedøgn \\
		\hline
		Pleievektsrelaterte aktiviteter & kr 23 000 000 & 20 000 pleievekter & 1 150 per pleievekt \\
		\hline
		Totalt & kr 41 000 000 & ~ & ~ \\
		\hline
	\end{tabularx}
\end{center}

Deretter finner vi en kostnad for de tre pasientgruppene:

\begin{center}
	\begin{tabularx}{\linewidth}{| X | X | X | X |}
		\hline
		Aktivitet & Grå stær & Fjerning av mandler & Laryngektomi \\
		\hline
		Innleggelsesrelaterte aktiviteter & kr 4 000 & kr 4 000 & kr 4 000 \\
		\hline
		Liggedøgnsrelaterte aktiviteter & kr 5 000 & kr 2 000 & kr 17 000 \\
		\hline
		Pleievektsrelaterte aktiviteter & kr 8 050 & kr 4 600 & kr 27 600 \\
		\hline
		Total kostnad & kr 17 050 & kr 10 600 & kr 48 600 \\
		\hline
	\end{tabularx}
\end{center}

Vi ser at vi får helt andre kostnader for de ulike pasientgruppene om vi benytter et enkelt system med kun liggedøgn, eller om vi tar hensyn til at det er krevende å håndtere mange pasienter (innleggelseskostnader) og at ulike paisenter er ulikt krevende å pleie.

\subsection{Kostnadshierarkiet}

I ABC legges det vekt på at kompleksitet driver kostnader, så an produsent som produserer 100 produkter med volum på en million per produkt har høyere kostnader enn en produsent i samme bransje som produserer 10 produkter av 10 millioner hver. Eksempler på kompleksitetskostnader kan være:
\begin{center}
	\begin{tabular}{l l}
		Virsomhet & Kompleksitetsdriver \\
		\hline
		Høgskole & Antall ulike spesialiseringsretninger \\
		Sykehus & Antall ulike behandlingstilbud \\
		Flyselskap & Antall ulike ruter \\
		Bryggeri & Antall ulike produkter \\
		Bildeimportør & Antall ulike lagførte varer \\
	\end{tabular}
\end{center}

Tradisjonelt fokuserer man på stordriftsfordeler. Dette er ikke i strid med ABC, men man vektlegger at stordriftsfordeler kan oppveies av økt kompleksitet. Hovedfokuset bør være på produktspesifikke stordriftsfordeler, altså at gitt at man produserer et produkt så vil kostnaden per enhet falle når volumet øker. Tradisjonelle kalkyler skiller kun mellom faste og volumvariable kostnader. Begrepet ikke-volumbaserte fordelingsnøkler er innført for å markere at man tar hensyn til andre forhold enn produksjonsvolum i modelleringen av kostnadsstrukturen.

For å understøtte fokuset på at flere forhold driver kostnader enn produserte enheter, ble begrepet kostnadshierarki innført. Begrepet defineres som: \emph{en inndeling av ulike aktiviteter i ulike nivåer etter den faktor som er kostnadsdrivende}.

\begin{figure}
	\caption{Eksempel på kostnadshierarki}
	\label{fig:eksempel-kostnadshierarki-abc}
	\centering
		\includegraphics[width=0.8\textwidth]{Figures/eksempel-kostnadshierarki-abc}
\end{figure}

\begin{itemize}
  \item \emph{Enhetsbaserte aktiviteter:} Disse utføres hver gang en produktenhet tilvirkes. De kalles også for volumbaserte aktiviteter. Eksempler er antall som besvarer en eksamen, eller bruk av tog, som gir vedlikeholdskostnader.
  \item \emph{Seriebaserte aktiviteter:} Utføres hver gang en serieproduksjon planlegges eller igangsettes. Typiske eksempler er varebestillinger, materialhåndtering, interntransport, omstillinger av maskiner og vareforsendelser til kunder.
  \item \emph{Produktbaserte aktiviteter:} Utføres i tilknytning til produkttyper eller varianter. Eksempler er kvalitetskontroll hvor noen produkter krever mer kontroll enn andre. Tilsvarende har vi for produksjonsplanlegging hvor enkelte produkter krever langt mer detaljerte spesifikasjoner enn andre. Kostnadsdriver vil derfor være antall varianter av samme produkt, antall kvalitetskontroller utført osv. Det kan være lurt å gruppere en del markedsføringsaktiviteter som kundebaserte aktiviteter, som vi ser i neste kapittel.
  \item \emph{Bedriftsbaserte aktiviteter:} Omfatter generelle administrative kostnader, forsikring, eiendom, eiendomsskatt, avgifter, vedlikehold. De kan ikke knyttes direkte til det enkelte produkt eller mengden som tilvirkes uten å gå veien om tilleggssatser.
\end{itemize}

Kostnadshierarkiet synliggjør produktspesifikke stordriftsfordeler, altså at man får lavere enhetskostnader dersom man produserer i store serier og med høye produktvolumer. ABC fremstiller dermed ofte lavvolumprodukter som dyre, og trekker oppmerksomheten i retning av å redusere produktspekteret.

\subsection{Kostnader for ledig kapasitet}

Variasjon i kostnader er ikke bare avhengig av kostnadenes natur, men også organisasjonens evne til å tilpasse kostnadsnivået til den totale arbeidsmengde. I ABC bør man skille mellom kostnader for ineffektivitet i organisasjonen og produkt- og tjenestekostnader. Man vil skille mellom ulønnsom drift og ulønnsomme produkter/tjenester.

Uttrekket av kostnader for inneffektivitet i ABC-metoden gjøres ved at man velger praktisk kapasitet som nevnervolum, dvs det volumet som aktivitetens kostnader divideres med for å finne kostnaden per kostnadsdriverenhet. Ledig kapasitet er \emph{differansen mellom faktisk kapasitetsutnyttelse og den praktisk mulige kapasitetsutnyttelsen under normale forhold.}

Eksempelvis kan man på et sykehus ha en operasjonsstue med følgende kapasitetsmål:
\begin{center}
	\begin{tabular}{l l}
		Teoretisk kapasitet & 6 000 timer \\
		Praktisk kapasitet & 5 000 timer \\
		Normal kapasitet & 4 800 timer \\
		Budsjettert kapasitet & 4 000 timer
	\end{tabular}
\end{center}

Forskjellen mellom praktisk og teoretisk er den tiden man trenger til oppgradering, rengjøring etc. Normalt utnytter man nesten optimalt, altså 4 800 timer. Neste år har man budsjettert med 4 000 timer. Dette skyldes ujevn tilgang på pasienter. Totalkostnaden er likevel den samme, 12 000 000 kr per år. Vi får følgende timepriser:
\begin{center}
	\begin{tabular}{l r r}
		Mål på kapasitet & Kapasitet & Timekostnad \\
		\hline
		Teoretisk & 6 000 timer & 2 000 per time \\
		Praktisk & 5 000 timer & 2 400 per time \\
		Normal & 4 800 timer & 2 500 per time \\
		Budsjettert & 4 000 timer & 3 000 per time		
	\end{tabular}
\end{center}

I ABC legger man vekt på å bruke 2 400 kr per time. Dersom man lar timekostnaden stige med fallende kapasitetsutnyttelse, skyver man den ledige kapasiteten over på produktene. Ifølge ABC er det ikke riktig å hevde at det blir dyrere å behandle pasienter når ledig kapasitet går opp. En av årsakene er at man kan risikere å redusere aktiviteten enda mer nrå man ser at pasientene er ``ulønnsomme''. 

Merk likevel at man ved bruk av ABC ikke får alle kostnadene fordelt ut på produktene. Dersom man faktisk ikke utnytter mer enn 4 000 timer, vil man få 9 600 000 som kostnader fordelt til produktene, og 2 400 000 kr som kostnad for ubenyttet kapasitet. Sistnenvte behandles som en bedriftsnivåkostnad.

\subsection{Fordeler og ulemper med ABC}

Det er fire åpenbare fordeler med ABC:
\begin{enumerate}
  \item Kalkylen blir mer nøyaktig ved at det brukes flere fordelingsnøkler og fordelingsgrunnlag for indirekte kostnader.
  \item Det benyttes andre typer fordelingsnøkler som bedre beskriver hva som er årsaken til dimensjoneringene av virksomhetens aktiviteter.
  \item Bedre styringsinformasjon ved at man får økt innsikt i hvor kostnadene oppstår i organisasjonen.
  \item Økt fokus på kapasitetsutnyttelse.
\end{enumerate}

\noindent
Vi har også noen ulemper:
\begin{enumerate}
  \item Høye kostnader for å registrere eller måle ressursbruken er sannsynligvis den største ulempen med ABC og representerer en begrensende faktor for bruken.
  \item Faste kostnader kan ikke gjøres variable selv om de fordeles iht kostnadsdrivere.
  \item Retorikken er sterk og kan virke mot sin hensikt.
\end{enumerate}

