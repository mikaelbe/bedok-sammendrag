\section{Økonomisk styring}

\subsection{Innledning}

Boken handler om økonomisk styring i det mellomlangsiktige og det kortsiktige
tidsperspektivet, det vi kaller de \emph{taktiske} og \emph{operative}
styringsnivåene. Når vi sier taktisk styring, tenker vi normalt på styring
gjennom året, mens det operative går på dag-til-dag styring.

\subsection{Bedrifters økonomiske målsetting}

Bedrifter er avhenige av løpende fornyede produkter og produktprosesser, som krever tilgang til risikovillig kapital. Det siste tiåret har begrepet \emph{aksjonærverdi} kommet, som innebærer at ledelsens oppgave er å lede virksomheten på en slik måte at eiernes/aksjonærenes forventede formue maksimeres, altså at avkastningen på egenkapitalen blir størst mulig. Det vil si størst mulig overskudd.

Avkastningen må tilfredsstille to hovedkrav

\begin{itemize}
  \item Den må være høyere enn innskuddsrenten i en bank, som er risikofri investering. Den må inneholde ekstraavkastning som reflekterer økt risiko.
  \item Den må minst representere det eierene kan få ved å investere sine penger i en annen næringsvirksomhet med tilvarende risiko, \emph{alternativkost ved å investere i virksomheten}.
\end{itemize}

Ledelsen bør imidlertid ikke føle seg presset til å gjøre valg som høyner aksjekursen på kort sikt men ikke lang sikt. De må ikke komme i konflikt med HMS-messige forhold. Man må også tenke på kundeverdi og tilfredsstille kunden gjennom produkters \emph{forventede verdi}.


\subsection{Økonomisk styring}

Skal skape en funksjonell sammenheng og samspill i organisasjonen. Det kan imidlertid oppstå målkonflikter, kommunikasjonsproblemer. Vi trenger godt samarbeid, motivasjon og en god bedriftskultur som støtter opp om for eksempel budsjettdisiplin. Økonomisk styring omfatter målformulering, styring og kontroll med alle aktiviteter, som på kort og lang sikt påvirker virksomhetens avkastning og likviditet, og som inneholder ansatte på alle nivåer i organisasjonen.

Ledelsen styrer taktisk planlegging, deriblant årsbudsjettene. En viktig del av oppfølgingsprosessen er å gjennomføre analyse av de avvikene som har oppstått mellom det som var planlagt og det som virkelig skjedde, altså \emph{avviksanalyser}.

Vi snakker også om jakten på handlingsalternativer. Avviksanalyene som gjennomføres kan konkludere med at større endringer må iverksettes for å bringe virksomheten på den målsatte kursen. Det er viktig at konsekvensene av valg er kartlagte før beslutninger treffes.

Sentralt er \emph{produktkostnader}, som benyttes både i forkalkyler, etterkalkyler og i lønnsomhetsanalyser. Man må også fremskaffe beslutningsrelevant informasjon knyttet til virksomhetens ulike kunder, avdelinger og andre forhold som vil stå sentralt. Det trengs også informasjon av ikke-finansiell karakter, for eksempel antall kundebesøk, leveransemønster, utviklingsprosjekter og kvalitet. Styringsparametre går gjerne i retning av mindre finansiell grad jo lenger ned i organisasjonen vi kommer. En sentral lederoppgave vil være å skreddersy det interne styringssystemet slik at det fremskaffer den informasjonen som virksomhetens medarbeidere trenger for å ha kontroll med utvikling og treffe gode beslutninger.

God økonomistyring dreier seg også om holdninger. Gode lederholdninger, god økonomisk forståelse hos medarbeidere og god budsjettdisipling, samt evnen og viljen til å skape størst mulige verdier.

\subsection{Ledelse og styring}

Vi har to totalt motsatte syn: markedsteorien og planleggingsteorien.

\begin{center}
	\begin{tabularx}{0.8\linewidth}{ | X | X | }
		\hline
		\textbf{Markedsteorien} & \textbf{Planleggingsteorien} \\
		\hline
		Ledelsen føyer seg ene og alene etter de gjeldende økonomiske, sosiale og politiske forhold.
		&
		Den fremtidige skjebne for virksomheten kan påvirkes, den kan planlegges og styres.
		\\ \hline
		
		Som en konsekvens er ledelsens rolle som spåmannens, den forsøker å spå om fremtiden ved å fortolke dagens situasjon.
		&
		Gode ledere klekker ut realistiske handlingsplaner for å nå bedriftens mål. 
		\\ \hline
		
		Når dagens situasjon er vurdert, tar ledelsen beslutninger.
		&
		Ledere kan påvirke en rekke variabler og tilpasse seg eller planlegge hvordan bedriften skal måte de variabler som ikke kan påvirkes.
		\\ \hline
		
		Ledelsens kompetanse bedømmes følgelig iht deres evne til å fortolke og reagere ut fra dagens situasjon.
		&
		Kvaliteten på bedriftens planer vil derfor sterkt påvirke utfallet av ledelsens løpende beslutninger.
		\\ \hline
		
		\textbf{Reaktive beslutninger:} Ledelsen tolker og reagerer iht dagens hendelser.
		&
		\textbf{Proaktive beslutninger:} Ledelsen kommer fremtidige hendelser i forkjøpet ved å planlegge for dem.
		\\ \hline
		
		\hline
	\end{tabularx}
\end{center}

\subsection{Budsjettet}

I tråd med vårt proaktive ledelsessyn er budsjettet en omfattende plan, som omsetter tall i de forventede konsekvenser og resultater av alle de planer og tiltak den skal iverksette for en gitt periode. Tallmaterialet har liten verdi isolert sett, og må sees i sammenheng med strategier og mål, \emph{gjennomføringsplaner}.

Budsjettet bør sees på som slutten på en prosess, som innebærer å se på strategier og strategiske mål, lønnsomhets- organisasjons- og markedsanalyser, taktiske budsjettmål og de enkelte avdelinger, utarbeidelse av handlingsplaner for å nå målene og etablering av kontroll- og oppfølgingsrutiner.

\subsection{Hva skal styres?}

Utgangspunktet er at det er knapphet på ressurser som kapital, arbeidskraft, kunnskaper og råmaterialer. Oppgaven blir å søke å optimalisere bruken for størst mulig avkastning. Dette kan vi for eksempel uttrykkes gjennom avkastningen på den kapital som arbeider i virksomheten, gjennom totalkapitalrentabilitet eller avkastningen på sysselsatt kapital. For å gjøre dette må vi måle virksomhetens kvalitet og nytte i forhold til noe.

Ressursstyring handler om å sikre virksomheten best mulige resurser til lavest mulig kostnad. Produktivitetsstyring handler om produktivitet, som defineres som “produksjonsmengde per ressursforbruk” eller “input per output”, altså å utnytte ressursene best mulig. Effektivitetsstyring handler om produktets eller tjenestens verdi eller nytte for forbrukeren. Vi skiller på indre og ytre effektivitet, hvor den indre fokuserer på interne forhold og går ut på at enkelte avdelinger skaper nytte eller verdier i forhold til “interne kunder” i bedriften. Den ytre effektivitet er knyttet til virksomhetens markedsside, og salg mot lønnsomme produkt- og markedssegmenter.

\subsection{Driftsregnskapet}

Driftsregnskapet eller internregnskapet har som hovedoppgave å gi periodisk detaljert regnskapsinformasjon om driften. Det sammenfatter løpende etterkalkulasjon som kontrolleres mot forkalkylene.

\subsection{Ulike typer driftsregnskap}

Ordreregnskap for serieproduserende virksomheter, for bedrifter som tilvirker ulike produkter i serier i store volum. Produktkostnadene har stort fokus i et ordregenskap. Det mest brukte regnskapet for denne type virksomheter er basert på kostnadsstandarder.

Ordreregnskap for ordreproduserende virksomheter, må kunne skille mellom kostnadene som medgår til de enkelte produkter og tjenester, for så å akkumulere dem per ordre, klient eller kunde. Disse virksomhetene driver normalt med “skreddersøm” for oppdragsgivere.

Prosessregnskap passer for bedrifter som produserer ett produkt i en lengre periode. De bør finne ekvivalensenheter av de enkelte produkter og ivareta kostnadsakkumulasjon i de enkelte avdelinger eller prosessområder.

Prosjektregnskaper passer for byggeprosjekter og prosessmoduler til oljevirksomheten. Sentralt er milepæler, som representerer tidspunkter for kostnadsavregninger og kontroll med fremtiden.