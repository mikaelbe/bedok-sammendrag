\section{Kostnadsstandarder}

I normalkostregnskapet ble de direkte kostandene for materialer og lønn ført ut fra virkelige forbrukstall, mens vi innkalkulerte de indirekte kostnader basert på normalsatser. Derfor vil det i kostnadsoppfølgingen i normalregnskapet ikke settes krav til hvor store de direkte kostnadene skal være i forhold til aktivitetsnivået eller budsjettet. For bedrifter som driver med gjentagende produksjon eller serieproduksjon trenger vi løpende kontroll med de direkte kostnadene. Bruken av kostnadsstandarder gir dette.

\subsection{Generelt om standarder}

\emph{En standard kan defineres som et målsatt krav til verdi som utførelsesverdien kan kontrolleres mot.} I bedrifter gis de enkelte komponenter sine bestemte krav til mengde og kvalitet hva gjelder råmaterialer per enhet. Fordelingen av hver produktenhet krever en viss bearbeidingstid i forskjellige maskiner og arbeidssteder, og det utarbeides standarder for hva foredlingstiden skal være osv. \emph{Tids- og stykkakkorder}  er eksempler på standarder for bearbeidingstid (standard tid).

En kostnadsstandard kan defineres som \emph{et målsatt krav til verdien av to komponenter: mengde av en produksjonsfaktor $\times$ pris per mengdeenhet}.

For direkte lønn utregnes standarden som summen av den tid som medgår i tilvirkingen av en produktenhet $\times$ lønssats per tidsenhet.
\begin{equation}
	\text{Standard lønnskostnad per enhet} = \text{Standard tid} \times \text{standard lønnssats}
\end{equation}

Tilsvarende for materialer:
\begin{equation}
	\text{Standard materialkostnad per enhet} = \text{Standard mengde} \times \text{standard pris}
\end{equation}

\emph{Kostnadsstandarder og budsjetter er to sider av samme sak: En kostnadsstandard uttrykker den budsjetterte (forkalkulerte) kostanden av hver produktenhet, mens budsjettet uttrykker totalkostnadene for det budsjetterte antall produktenheter.} Forskjellen er altså mengden, antall produktenheter, som beskrives.

\subsection{Formålet med kostnadsstandarder}

Et av driftsregnskapenes formål er løpende kontroll med driftsutviklingen og standardene. Først fastsettes standarden for kostnadenes størrelse per enhet. Standardkost per enhet multipliseres deretter med antall enheter som produseres i perioden, og vi får periodens standardkostnader for den virkelige mengden. De virkelige realiserte kostnadstallene i perioden regnskapsføres og sammenliknes mot standardkostnadene, og avvikene registreres. Vi kan da sammenlikne uten å ta hensyn til mengde som faktisk ble produsert. Standardkostnadene kan deretter oppdateres, om ønskelig.

Brukere av kostnadsstandarder er serieproduserende tilvirkingsbedrifter.

Hovedformålet med bruken av kostnadsstandarder er å trekke oppmerksomheten mot virksomhetens produktivitetsutvikling -- både (1) gjennom det arbeidet som gjøres når kostnadsstandardene fastsettes, og (2) gjennom den periodiske oppfølgingen av avvik.

Blant andre formål finner vi
\begin{itemize}
	\item Forenklet budsjettering
	\item Prisfastsettelse og lønnsomhetsanalyser
	\item Kostnadskontroll og avviksanalyse av både direkte og indirekte kostnader
	\item Enklere og mer stabil verdivurdering av varer i arbeid (ViA) og ferdigvarer
	\item Raskere og enklere utforming av driftsregnskapet
\end{itemize}

\subsection{Avviksanalysen}

Standardkostnadene er de planlagte og forventede kostnadene ved periodens virkelige produksjonsvolum. Når virkelige kostnader foreligger, sammenliknes de. Ledernes oppfølgingsoppgaver konsentreres om kostnadsavvikene. Det brukes liten tid dersom kostnadsavvikene er små. Gjennom kortperiodiske resultatrapporter som viser standardkostnader, virkelige tall og avvik, analyserer man avvikene og initierer de tiltak som må igangsettes.

\subsection{Fastsettelse av standardkostnader}

Fastsettelse av standardkostnader er normalt en krevende oppgave, og blir gjort gjennom samarbeid mellom personer og avdelinger i organisasjonen. God ledelse tilsier at de som berøres av standarden også bør være med på å fastsette standardens størrelse.

Skal en bedrift fastsette en tidsstandard for første gang, starter man med \emph{tidsstudier}. Det er også vanlig at enkelte spesielt kyndige arbeidere fastsetter \emph{akkorden}. Der hvor oppgaven er å vedlikeholde en tidsstandard for å budsjettere eller fastsette en fremtidig standard, vil grunnlaget være dens historiske utvikling.

De nye standarder skal reflektere en fremtid hvor bedriften legger inn realistiske målsettinger om forbedret effektivitet og produktivitet.

\emph{Perfeksjonsstandarder} blir fastsatt på basis av ideelle driftsforhold, som 100 \% maskinproduktivitet, de dyktigste arbeiderene og et forbruk uten svinn eller vrak. Argumentene for å fastsette på denne måten er at standarden skal være en løpende påminnelse om nødvendigheten for stadige forbedringer. De negative er imidlertid større enn de positive virkningene, ved at selv de dyktigste arbeiderene aldri klarer å oppnå de kravene bedriften har satt. Det blir dessuten vanskelig å skille mellom normale og unormale avvik.

\emph{Oppnåelige standarder} skal også være noe å strekke seg mot, men baseres på normale krav til effektivitet og produktivitet for budsjettperioden. Både budsjetter og planer uttrykker hva som er forventet i den fremtidige periode dersom driften utvikler seg normalt.

\subsection{Fastsettelse av standard lønnskost}

Fastsettelse av standard lønnssats vil gjennomføres på ulike måter i bedrifter. Det kan etableres en felles standard timelønnssats for hele bedriften, eller det kan være aktuelt å lage ulike lønnssatser for kategorier av arbeidere. En vanlig metode er å regne ut gjennomsnittelig timelønn for alle som arbeider i en avdeling.

Fastsettelse av standard tid per produktenhet er normalt vanskeligst, siden den handler om den enkelte arbeiders innsats. Mellomstore og større industribedrifter har egne avdelinger eller medarbeidere som kun arbeider med å planlegge produksjonsprosessen til minste detalj. Den standard tilvirkingstid som fastsettes må også inkludere tid til kaffepauser, personlige behov, rengjøring og maskinstopp. Nedenfor ser man et typisk eksempel på en standard lønnskost.
\begin{center}
	\begin{tabular}{ l l }
		Timelønn & 212,00 \\
		Bev helligdager (4,5\%) & 9,54  \\ \hline
		~ & 221,54 \\
		Feriepenger (12\%) & 26,58 \\ \hline
		~ & 248,12 \\
		Arbeidgiveravg (14,1\%) & 34,99 \\ \hline
		Brutto lønnskostnad & 283,11 \\
		Andre lønnskostnader (6\%) & 16,99 \\ \hline
		Standard lønnsats per time & 300,10
	\end{tabular}
\end{center}

\subsection{Fastsettelse av standard materialkost}

Foruten prisen per enhet må standarden inneholde alle bedriftens kostnader forbundet med å bringe råvarende inn på lager, inklusive rabatter. Dette kaller vi \emph{inntakskost}.

Den standard mengde bedriften fastsetter som det forventede forbruk av direkte materialer, må inneholde det medarbeiderene har beregnet medgår i hvert enkelt produkt, pluss svinn og kassasjon.

\subsection{Fastsettelse av standarder for de indirekte variable kostnader}

De indirekte, variable kostandene i en industribedrift omfatter for eksempel lakk, lim, smøreoljer (hjelpemateriell) og driftsmateriell som det er vanskelig å detaljregistrere og knytte til den enkelte kostnadsbærer. Standarder for de indirekte variable kostnadene fastsettes i henhold til det prinsipp og den metode som vi benyttet for å fastsette normalsatsen for de indirekte kostnader. Det er dermed ingen prinsipiell forskjell på en standardsats eller normalsats for de indirekte kostnadene. Begge er forkalkulerte satser for innkalkulering av de indirekte kostnader. Fordelingsgrunnlaget vil være materialforbruk, maskintimer eller arbeidstimer.

\subsection{Driftsbudsjettering med koststandarder}

Vi bruker standardkost per enhet og ganger med budsjettert mengde.

I mellomstore og større bedrifter har vi normalt formaliserte innkjøpsbudsjetter som koordinerer de enkelte avdelingers materialbehov. De er satt opp med bakgrunn i produksjonsplanene og endringene av råvarebeholdning som er lagt opp til. Vi kjøper inn basert på budsjettert innkjøpsmengde.

\begin{center}
	\begin{tabular}{ c l }
		~ & Budsjettert produksjon i materialmengde \\
		- & IB råvarer \\
		+ & budsjettert UB råvarer \\
		\hline
		= & budsjettert innkjøpsmengde
	\end{tabular}
\end{center}

\noindent
Standard tid per produktenhet dimensjonerer direkte det antall operatører som trengs for å gjennomføre den budsjetterte produksjonen. Dette tilsvarer \emph{brutto foredlingstid}, altså at tiden også inkluderer personlig tid, kaffepauser, interne møter og liknende.

Et standardkostregnskap vil vise hva den virkelige produksjonsmengden i en gitt periode skal ha kostet. Hvis man i en periode har registrert en avvikende tilvirket mengde, justerer man dette.

Kun signifikante avvik skal analyseres. Dette kan finnes ut via det prosentvise avviket i forhold til budsjettet. Den enkelte leder legger også andre faktorer til grunn for avviksfokuseringen.

\subsection{En modell for avviksanalyse}

Avvikene fremkommer i materialavvik og lønnsavvik. Siden de består av to komponenter, vil totalavviket ha to forklaringer: avvik i mengde og avvik i pris. Vi må utvikle en modell som isolerer pris- og mengdekomponentene. Dette gjelder også for tid i stedet for mengde, og sats i stedet for pris. Vi kan sette opp:

\begin{align}
	\text{Produktivitetsavvik} &= \text{std pris} \times (\text{std mengde} - \text{virkelig mengde}) \\
	\text{Faktorprisavvik} &= (\text{std pris} - \text{virkelig pris}) \times \text{virkelig mengde} \\
	\text{Totalavvik} &= \text{std pris} \times \text{std mengde} - \text{virkelig pris} \times \text{virkelig mengde}
\end{align}

\noindent
Produktivitetsavviket gir svar på om vi har brukt mer eller mindre av mengdekomponentene (tid og materialer). Faktorprisavviket er avviket i pris per enhet. 

Man kan se på produktivitetsavvik som standardkostnader, gitt med standard pris/sats ganger standard mengde/tid, minus tallene fra avviksanalysen, som er gitt av standard pris/sats ganger virkelig mengde/tid.

Faktorpris (prisavvik/lønnssatsavvik) kan sees på som de virkelige kostnadene, som er gitt av virkelig pris/sats ganger virkelig mengde/tid, minus tallene fra avviksanalysen.

Totalavvik (materalavviket og lønnsavviket) kan sees som de virkelige kostnadene minus standardkostnaden. 

\subsection{Materialregnskapet}

Vi kaller den delen av driftsregnskapet hvor vi fører alle transaksjoner som har med materialene å gjøre for materialregnskapet. Når vi skal føre direkte materialkostnader i standardkostregnskapet, benytter vi en standard materialkost per enhet multiplisert med antall enheter som faktisk er produsert.

\begin{equation}
	\text{Standard materialkost} = \text{standard materialkost per enhet} \times \text{virkelig antall produsert}
\end{equation}

Vi beregner hvor mye materialkostnadene \emph{skulle ha vært} for virkelig produksjonsmengde. Vi kan deretter gjøre analyser basert på avviket. Vi kan bryte det opp i mengdeavvik, prisavvik for direkte materialer og materialavvik.

\subsection{Lønnsregnskapet}

\begin{equation}
	\text{Standard lønnskost} = \text{standard lønnskost per enhet} \times \text{virkelig antall produsert}
\end{equation}

Vi beregner hvor mye lønnskostnadene skulle ha vært for den virkelige produksjonsmengde, og bryter standarden opp i mangde- og pris-komponenten for å beregne standard tid for den virkelige mengde separat. Vi finner da lønnsavviket og produktivitetsavviket, samt lønnssatsavviket.

\subsection{Avvik i de indirekte variable kostnader}

Det ufordelaktige avviket mellom standard og virkelige indirekte variable kostnader kan enten ha sammenheng med kostnadssløsing eller dårligere produktivitet i avdelingen. Vi kan bryte opp dette i to avvik: produktivitetsavviket og forbruksavviket. Produktivitetsavvik i de indirekte kostnadene oppstår som en følge av produktivitetsavvik i fordelingsgrunnlaget.

\begin{equation}
	\text{Produktivitetsavvik} = \text{standard sats} \times (\text{standard tid} - \text{virkelig tid})
\end{equation}

\begin{equation}
	\text{Forbruksavvik} = \text{standard sats ind VK} \times \text{virkelig tid} - \text{virkelige indirekte VK} 
\end{equation}

Legg merke til at vi normalt ikke splitter opp de virkelige indirekte variable kostnadene opp i en mengdekomponent og en priskomponent.