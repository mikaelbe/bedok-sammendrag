\section{Standardkostregnskapet}

Hvordan den enkelte bedrift leger opp føringen av standardkostregnskapet er avhengig av formål. Mange bedrifter fører standardkostregnskapet som et selvstendig regnskap uavhengig av finansregnskapet, mens andre har det som en integrert del av et samlet regnskapssystem. Vi skal benytte tabellform, tilsvarende den form som vi benyttet i føringen av normalkostregnskapet.

\subsection{Budsjettforutsetninger}

Vi skal føre et fullstendig standardkostregnskap, først etter selvkostprinsippet og deretter etter bidragsprinsippet. Det totale kostnadsbudsjettet for de direkte kostnadene bygges opp av standardkalkylens direkte kostnader multiplisert med den mengde som planlegges tilvirket av hvert produkt, mens standardsatsene som brukes i produktkalkylene beregnes ut fra totalbudsjettets tall.

Hvis vi vet det budsjetterte aktivitetsnivået for året og har utarbeidet planer som skal gjennomføres, kan vi budsjettere de indirekte variable og faste kostnadene. De indirekte variablene er knyttet til aktivitetens størrelse, men er ikke registrerte i detalj.

Som drøftet i forrige kapittel er det de forkalkulerte standardkostnadene uttrykt gjennom \emph{standardkalkylen} som legges til grunn for den periodiske styringen gjennom året. I den løpende oppfølgingen måler vi utviklingen mot periodens standardkostnader, definert som standardkalkylen (per enhet) multiplisert med virkelig produksjonsmengde. \emph{Standardkostnaden blir dermed de aktivitetskorrigerte budsjettallene.}

\subsection{Standardkalkylen etter selvkostprinsippet}

Vi starter med å utarbeide aktivitetsmål (kostnadsdrivere) som best forklarer de enkelte produkters ressursbruk. Dette kan være eksempelvis:
\begin{center}
	\begin{tabular}{l l}
		Materialavdeling & Antall kg \\
		Tilvirkingsavdeling & Direkte arbeidstimer \\
		Salgs- og adm.avdeling & Tilvirkingskost solgte varer / solgte enheter \\
	\end{tabular}
\end{center}

I selvkostkalkylen vil vi som tidligere fordele summen av de indirekte kostnader på kostnadsgruppen/avdelingen og viderefordele dem til de enkelte kalkyleobjekter ut fra et årsaks-/virkningsprinsipp. Vi fordeler de indirekte kostnadene på tilsvarende måte som i normalregnskapet, men kaller som kjent satsen nå for standardsats. Beregningsmåten er nøyaktig den samme. Hvis aktivitetsmålet er arbeidstimer blir standardsatsen:
\begin{equation}
	\text{Standardsats per time} = \frac{\text{Budsjetterte indirekte kostnader i avdelingen}}{\text{Budsjeterte direkte arbeidstimer i avdelingen}}
\end{equation}

For eksempel kan standardsats for de indirekte variable materialkostnadene være:
\begin{equation}
	\frac{\text{kr 66 000}}{\text{13 200 kg}} = \text{kr 5 per kg}
\end{equation}

Vi har nå grunnlag for å kalkulere standard tilvirkingskost per enhet for bedriftens produkter, altså \emph{standardkalkylen} basert på selvkost, for eksempel:

\begin{center}
	\begin{tabular}{l c r}
		\emph{Direkte kostnader:} 	& ~ 				& ~ 	 \\
		Direkte materialer			& 3 kg à kr 55 		& kr 165 \\
		Direkte lønn:				& ~					& ~		 \\
		\hspace{5 mm} Tilvirkingsavd 1 & 1.5 t à kr 300 & kr 450 \\
		\hspace{5 mm} Tilvirkingsavd 2 & 1 t à kr 280   & kr 280 \\
		\\
		\emph{Indirekte tilvirkingskostnader:} & ~ & ~ \\
		Materialavdeling			&  3 kg à kr 25		& kr 75 \\
		Tilvirkingsavdeling 1		& 1.5 t à kr 80		& kr 120 \\
		Tilvirkingsavdeling 2		& 1 t à kr 120		& kr 120 \\
		\hline
		\emph{Standard tilvirkingskost} & ~ & kr 1210 \\
		Indirekte salgs- og adm.kostnader: 1210*0.20 & ~ & kr 242 \\
		\hline
		\emph{Standard selvkost}	& ~ & kr 1452\\
	\end{tabular}
\end{center}

Hvis vi introduserer salgspirs per enhet og trekker fra standardselvkost, finner vi forkalkulert fortjeneste per enhet:
\begin{center}
	\begin{tabular}{l r}
		Salgspris & kr 1650 \\
		Standard selvkost & kr 1452 \\
		\hline
		Forkalkulert fortjeneste per enhet & kr 198 \\
	\end{tabular}
\end{center}

\subsection{Standardkostregnskapet etter selvkostprinsippet}

Et standard selvkostregnskap ført i tabellform bygger på vår tidligere kalkulasjonsmal:
\begin{center}
	\begin{tabular}{l l}
		~ 	& Standard direkte materialer \\
		+	& \emph{Standard indirekte kostnader i materialavdelingen} \\
		+	& Standard direkte lønn \\
		+	& \emph{Standard indirekte kostnader i tilvirkingsavdelingene} \\
		\hline
		=	& Kalkulert tilvirkingskost i perioden \\
		-/+	& \emph{Beholdningsendringer varer i arbeid (økning -/reduksjon +)} \\
		\hline
		=	& Kalkulert tilvirkingskost for ferdigproduserte varer \\
		-/+	& Beholdningsendringer ferdigvarer (økning -/reduksjon +) \\
		\hline
		=	& Kalkulert tilvirkingskost solgte varer \\
		+	& Innkalkulerte indirekte kostnader salg/administrasjon \\
		\hline
		=	& Kalkulert selvkost
	\end{tabular}
\end{center}

\begin{center}
	\begin{tabular}{l l}
		~	& Virkelige salgsinntekter \\
		-	& \emph{Kalkulert selvkost} \\
		\hline
		=	& Produksjonsresultat \\
		+	& \emph{Avvik direkte kostnader} \\
		+	& \emph{Dekningsdifferanser} \\
		\hline
		=	& Produksjonsresultat
	\end{tabular}
\end{center}

Kalkulert tilvirkingskost solgte varer kommer etter at tilvirkingskost er korrigert for beholdningsendringene i varer i arbeid og ferdigvarelager, også kalkulert på basis av standardkalkylene. Kalkulert selvkost representerer de totale standardkostnadene for hvert av de solgte produktene.

Produktresultatet viser det kalkulerte resultatet per produkt og totalt for perioden, mens avvikene i de direkte kostnadene og dekningsdifferanse gjelder bedriften totalt.

Når man arbeider med et standardkostregnskap, blir tilvirkingskostnadene som registreres (produktkostnadene) regnskapsført til standardkost. Vi trenger altså ingen virkelige kostnadstall for å beregne periodens produktresultat. \emph{Det betyr at bedriften i prinsippet kan beregne og regnskapsføre et foreløpig resultat -- produktresultatet -- med én gang periodens salgs- og produksjonsvolum er kjent.} Dette kan bety på daglig basis.

Når finansregnskapets tall etter en tid kommer, kan de virkelige kostnadstallene trekkes inn i driftsregnskapet for å sammenlikne med de standardkostnadene som allerede er registrert. Man kan analysere signifikante avvik.

Et annet viktig formål med standardkostregnskapet er å ha et system for å kontrollere de virkelige kostnader mot de planlagte.

\subsection{Føringen av standardkostregnskapet etter selvkostprinsippet}

For å utarbeide standardkostregnskapet bruker vi følgende fremgangsmåte:
\begin{enumerate}
  \item Beregne standardsatsene og utarbeide standardkalkylen.
  \item Beregne standard mengde og standard tid for den virkelige produksjonen.
  \item Beregne beholdningsendringene i varer i arbeid og ferdigvarer.
  \item Beregne produktresultatet for de enkelte produktene og totalt.
  \item Registrere de virkelige direkte og indirekte kostnadene, og beregne produksjonsresultatet.
  \item Analysere avvikene for de direkte og indirekte kostnadene.
\end{enumerate}

Selve nøkkelen til standardkostregnskap ligger i beregningen av standard mengde og standard tid. Grunnlaget er virkelig produksjonsmengde, og vil normalt bestå av både helt og delvis ferdig tilvirkede produkter. \emph{For å finne den virkelige produksjonen i helt eller delvis ferdig tilvirkede enheter må vi korrigere for eventuelle beholdningsendringer ved inngangen til og utgangen fra den perioden vi skal avlegge driftsregnskap for.} Dette registreres som varer i arbeid og ferdigvarer. Det kan være ulike trinn av varer i arbeid.

Standard mengde for perioden er den materialmengden som \emph{skulle ha vært forbrukt iht de forkalkulerte satser}.

Standard tid for perioden er den arbeidstid som \emph{skulle ha vært brukt iht de forkalkulerte satser}.

I beregningene, hvis vi har etterfølgende avdelinger (tilvirkingsavdeling 1 og 2), kan vi ha forskjellige antall produserte enheter i disse, da noen kan ha vært gjennom begge, og noen produkter kan være uferdige og bare ha vært gjennom én.

For føringen av selve regnskapet henvises det til side 155 i boken.

Merk at \emph{standard mengde for prdouktet beregnes som forkalkulert mengde per enhet multiplisert med virkelig produksjonsmengde}.
\begin{equation}
	\text{Standard mengde} = \text{forkalkulert mengde per enhet} \times \text{virkelig produksjonsmengde}
\end{equation}

For å registrere de direkte og indirekte kostnadene og beregne produksjonsresultatet, avstemmer vi ordinære finansregnskapets virkelige tall mot standardkostnadene. De virkelige regnskapstallene føres inn i sine respektive rader i kolonnen for virkelige kostnader, og de direkte kostnadsavvikene og dekningsdifferansene kan beregnes. Produksjonsresultatet -- det virkelige resultatet -- beregnes som produksjonsresultat pluss avvik direkte og indirekte kostnader.

\subsection{Standardkalkylen etter bidragsprinsippet}

De fleste elementene er like som ved selvkostprinsippet, men i bidragsregnskapet blir de faste kostnadene behandlet som periodekostnader. Vi kan sette opp et eksempel på en standard bidragskalkyle:

\begin{center}
	\begin{tabular}{l c r}
		\emph{Direkte kostnader} \\
		\hspace{5 mm} Direkte materialer	& 3 kg à kr 55 	& kr 165 \\
		Direkte lønn: \\
		\hspace{5 mm} Tilvirkingsavdeling 1	& 1.5 t à kr 300 & kr 450 \\
		\hspace{5 mm} Tilvirkingsavdeling 2	& 1 t à kr 280 & kr 280 \\
		\emph{Indirekte variable tilvirkingskostnader:} \\
		\hspace{5 mm} Materialavdeling		& 3 kg à kr 5 & kr 15 \\
		\hspace{5 mm} Tilvirkingsavdeling 1	& 1.5 t à kr 20	& kr 30 \\
		\hspace{5 mm} Tilvirkingsavdeling 2 & 1 t à kr 30	& kr 30 \\
		\hline
		Standard tilvirkings\emph{mer}kost 	& ~ & kr 970 \\
	\end{tabular}
\end{center}

Ved å multiplisere det budsjetterte salgsvolum for de enkelte produktene med standard tilvirkings\emph{mer}kost per enhet finner vi totalt budsjettert tilvirkings\emph{mer}kost. Vi kan da legge til variable salgskostnader og få salgs\emph{mer}kost. Salgsmerkost pluss dekningsbidrag blir salgsprisen.

\subsection{Føringen av standardkostregnskapet etter bidragsprinsippet}

Standard bidragsregnskap i tabellform kan bygges opp som vist nedenfor:

\begin{center}
	\begin{tabular}{l l}
		~ 	& Standard direkte materialer \\
		+	& Standard direkte lønn \\
		+	& \emph{Standard indirekte variable kostnader i material- og tilvirkingsavd} \\
		\hline
		=	& Kalkulert tilvirkings\emph{mer}kost i perioden \\
		-/+	& \emph{Beholdningsendringer varer i arbeid (økning -/reduksjon +)} \\
		\hline
		=	& Kalkulert tilvirkings\emph{mer}kost for ferdigproduserte varer \\
		-/+	& Beholdningsendringer ferdigvarer (økning -/reduksjon +) \\
		\hline
		=	& Kalkulert tilvirkings\emph{mer}kost solgte varer \\
		+	& Innkalkulerte indirekte variable kostnader salg/administrasjon \\
		\hline
		=	& Kalkulert salgs\emph{mer}kost
	\end{tabular}
\end{center}

\begin{center}
	\begin{tabular}{l l}
		~	& Virkelige salgsinntekter \\
		-	& \emph{Kalkulert salgsmerkost} \\
		\hline
		=	& Kalkulert dekningsbidrag (per produkt) \\
		+	& \emph{Avvik direkte kostnader} \\
		+	& \emph{Dekningsdifferanser} \\
		\hline
		=	& Produksjonsresultat (totalt) \\
		-	& Budsjetterte faste kostnader \\
		+ 	& Budsjettavvik faste kostnader \\
		\hline
		=	& Produksjonsresultat
	\end{tabular}
\end{center}

\emph{Med kalkulert dekningsbidrag i standardkostregnskapet mener vi virkelige salgsinntekter minus kalkulert salgsmerkost.}

Kalkulert dekningsbidrag beregnes per produkt mens virkelig dekningsbidrag bare kan vises totalt for bedriften, da avvikene ikke vil kunne brytes opp ned på produkter. Produksjonsresultatet kan enten beregnes som ovenfor, eller som vist nedenfor:

\begin{center}
	\begin{tabular}{l l}
		~ & Virkelig dekningsbidrag \\
		- & Virkelige faste kostnader \\
		\hline
		= & Produksjonsresultat
	\end{tabular}
\end{center}

Skjema for standardkostregnskap etter bidragsprinsippet vil være svært likt tilsvarende skjema for selvkost. Det er kun de variable kostnadene som inkluderes som produktkostnader i bidragsregnskapet. For de faste kostandene brukes de budsjetterte kostnadene.

Varere i arbeid og ferdigvarer i bidragsregnskapet skjer på basis av standard tilvirkingsmerkost.

\subsection{Sammenlikning av resultatene etter selvkost- og bidragsprinsippet}

En bedrift som ikke har hatt beholdningsendringer av produserte varer vil få samme produksjonsresultat med de to metodene. Ved beholdningsendringer vil beholdningene vurderes iht tilvirkingskost når selvkostprinsippet benyttes, og tilvirkings\emph{mer}kost når vi bruker bidragsprinsippet.

Ved beholdningsøkninger blir produktkostnadene overført til etterfølgende periode. Når disse produktkostnadene også inkluderer faste tilvirkingskostnader, blir det mindre kostnader som belaster inneværende periode. Følgelig vil selvkostprinsippet gi et bedre resultat enn bidragsprinsippet.

Ved beholdningsreduksjoner vil selvkostregnskapet gi et dårligere resultat.