Sammendrag for TIØ4295 Bedriftsøkonomi
======================================

Dette er et sammendrag av boken Driftsregnskap og budsjettering og annet pensum
i faget Bedriftsøkonomi på NTNU.

**Bidra gjerne hvis du finner feil eller mangler, ved å skrive det inn eller
rapportere et issue!**

**Alt pensum er ikke nødvendigvis dekket -- bidra gjerne med mer!**